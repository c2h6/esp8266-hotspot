#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "credentials.h"
#include <WiFiClientSecureBearSSL.h>
using namespace BearSSL;
#define DEBUG true
 
const char* ssid = WIFI_SSID;
const char* password = WIFI_PASS;

const String TELEKOM_FP="FF F4 F8 43 10 C5 A1 44 3B F4 78 2A 83 DE A0 0E 97 7D ED AC";
const String HEISE_FP="EF DF F1 77 03 6A BA 3A 8E DF 58 8D DC 0A 7D 2E A1 A6 D9 9F";
const uint8_t fingerprint[20] = {0xFF, 0xF4, 0xF8, 0x43, 0x10, 0xC5, 0xA1, 0x44, 0x3B, 0xF4, 0x78, 0x2A, 0x83, 0xDE, 0xA0, 0x0E, 0x97, 0x7D, 0xED, 0xAC};

 
void setup () {
 
  Serial.begin(115200);
  WiFi.hostname("EthansESP07");
  WiFi.begin(ssid, password);
 //TODO: implement WifiMulti: https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/WiFiMulti/WiFiMulti.ino
 
  while (WiFi.status() != WL_CONNECTED) {
 
    delay(300);
    Serial.print(WiFi.status());
  }
}
 
void loop() {
 
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

//    testSSL();

    HTTPClient http;  //Declare an object of class HTTPClient

    Serial.println("Sending HTTP Request...");
//    http.begin("http://etadar.de");  //Specify request destination
    http.begin("http://heise.de");

    const char* headerNames[] = { "Location", "Set-Cookie" };
    http.collectHeaders(headerNames, sizeof(headerNames)/sizeof(headerNames[0]));

    int httpCode = http.GET();   
//    Serial.println(httpCode);   //Print HTTP return code//Send the request

    String loc;
    
    if (httpCode > 0) { //Check the returning code

      if (http.hasHeader("Location")) {
        loc = http.header("Location");
 //       Serial.println("Found Location in header: "+loc);
      }
      String payload = http.getString();   //Get the request response payload
      Serial.println(payload);                     //Print the response payload
//      delay(5000);
    }

    http.end(); // not sure if this is necessary

    if (httpCode=302) {
      Serial.println("Redirecting to "+loc+"...");

      BearSSL::WiFiClientSecure client;
      client.setFingerprint(fingerprint);
      HTTPClient https;
      https.begin(client, loc);
      https.collectHeaders(headerNames, sizeof(headerNames)/sizeof(headerNames[0]));
      int httpCode = https.GET();
      String payload = https.getString();
      Serial.println(payload);
      if (https.hasHeader("Location")) {
        loc = https.header("Location");
      }
      if (https.hasHeader("Set-Cookie")) {
        String cookies = https.header("Set-Cookie");
        Serial.println("Got these cookies: "+cookies);
      }
      https.end();

      if (httpCode=302) {
        Serial.println("Redirecting AGAIN to https://hotspot.t-mobile.net"+loc+"...");

        https.begin(client, "https://hotspot.t-mobile.net"+loc);
        https.collectHeaders(headerNames, sizeof(headerNames)/sizeof(headerNames[0]));
        httpCode = https.GET();
        payload = https.getString();
        Serial.println(payload);
      }
    https.end();   //Close connection      
    }

 

  }
 
  delay(60000);    //Send a request every 30 seconds
}


void testSSL() {
  BearSSL::WiFiClientSecure wifiSecure;
  HTTPClient client;
  client.begin(wifiSecure, "https://www.heise.de", 443);
  int resultCode = client.GET();
  String payload = client.getString();
  Serial.println("Result Code: "+(String)resultCode);
  Serial.println("Payload: "+payload);
}
