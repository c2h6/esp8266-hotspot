# esp8266-hotspot

This is an attempt to implement automatic logging into any Telekom public wifi hotspot that might be available.
It requires https and possibly wispr (still researching).

After connecting to a Telekom accesspoint, any http attempt is forwarded by a 302 to a login page which requires ssl, i.e.
https://hotspot.t-mobile.net/wlan/redirect.do?origurl=http%3A%2F%2Fetadar.de%2F&ts=1537278587186

The HTTPClient library actually accepts URL's with https but return a -1 errorcode.
Currently it looks to me that I need to implement something like this: https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/examples/HTTPSRequest/HTTPSRequest.ino

Some info on WispR: https://coova.github.io/development/2010/08/09/coovachilli-wispr-2-0.html
